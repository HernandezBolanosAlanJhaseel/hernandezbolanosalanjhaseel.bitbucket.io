var Ajedrez = (function() {
  var _mostrarTablero = function() {
    limparMensajes();
    console.log("entra en mostrarTablero");
    request();
  };

  var _actualizar_tablero = function() {
    limparMensajes();
    var div_Tablero = document.getElementById("tablero");
    if (div_Tablero.childNodes[0] != null) {
      div_Tablero.removeChild(div_Tablero.childNodes[0]);
    }

    request();
  };

  var _mover_pieza = function(obj_parametros) {
    limparMensajes();
    var de = obj_parametros.de;
    var a = obj_parametros.a;
    var espacio_de = document.getElementById(de);
    var espacio_a = document.getElementById(a);
    console.log(espacio_de);
    console.log(espacio_a);
    if (espacio_de != null) {
      if (espacio_a != null) {
        var value_De = espacio_de.textContent;
        var value_A = espacio_a.textContent;
        if (value_De == value_A &&  value_A != "" ) {
          //si de y a son el mismo valor
          mensajes("advertencia : debes seleciconar un destinto diferente al origen");
        } else {
          if (value_De != "") {
            if (value_A == "") {
              var temporal = value_A;
              espacio_a.textContent = value_De;
              espacio_de.textContent = temporal;

            } else mensajes("advertencia : 'a' debe ser un espacio vacio");
          } else mensajes("advertencia : 'de' debe ser una pieza en el tablero");
        }
        //si no exite el id en a o de
      } else mensajes("advertencia : instruccion 'a' no valida");
    } else mensajes("advertencia : instruccion 'de' no valida");
  };

  function request() {
    var url = "https://hernandezbolanosalanjhaseel.bitbucket.io/csv/tablero.csv"
https://hernandezbolanosalanjhaseel.bitbucket.io
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        if (this.status === 200) {
          var aje = this.responseText.split("\n");
          validarCSV(aje);
        } else {
          mensajes("error : no se pudo descargar tablero");
          console.log("error");
        }
      }
      Ajedrez
    };
    xhr.open('GET', url, true);
    xhr.send();
  }

  function validarCSV(datos) {
    var piezas = {
      torreN: 2,
      caballoN: 2,
      alfinN: 2,
      reyN: 1,
      reinaN: 1,
      peonN: 8,
      torreB: 2,
      caballoB: 2,
      alfinB: 2,
      reyB: 1,
      reinaB: 1,
      peonB: 8
    }

    if (datos.length === 10) {
      for (var i = 1; i < datos.length - 1; i++) {
        var fila = datos[i];
        var codePoint = fila.split("|");
        if (codePoint.length == 8) {
          for (var j = 0; j < codePoint.length; j++) {
            var value = codePoint[j].charCodeAt();
            switch (value) {
              case 9820: //torre negra
                piezas.torreN--;
                break;
              case 9822: //caballo N
                piezas.caballoN--;
                break;
              case 9821: //alfiL N
                piezas.alfinN--;
                break;
              case 9819: //reina N
                piezas.reinaN--;
                break;
              case 9818: //rey N
                piezas.reyN--;
                break;
              case 9823: //peon N
                piezas.peonN--;
                break;
              case 9814: //torre blanco
                piezas.torreB--;
                break;

              case 9816: //caballo B
                piezas.caballoB--;
                break;
              case 9815: //alfiL B
                piezas.alfinB--;
                break;
              case 9813: //reina B
                piezas.reinaB--;
                break;
              case 9812: //rey B
                piezas.reyB--;
                break;
              case 9817: //peon B
                piezas.peonB--;
                break;
              default:
            }
          }

        } else mensajes("error : archivo csv imcompleto");
      }
      //fin for
      if (piezas.torreB ==0 && piezas.caballoB ==0 && piezas.alfinB ==0 && piezas.reyB ==0 && piezas.reinaB ==0 && piezas.peonB ==0&&
         piezas.torreN ==0 && piezas.caballoN ==0 && piezas.alfinN ==0 && piezas.reyN ==0 && piezas.reinaN ==0 && piezas.peonN ==0){
           console.log("numero de piezas correcto");
                     crearTablero(datos);
      }else mensajes("error : numero de piezas incorrecto");

    } else mensajes("error : archivo csv incompleto");

  }

  function crearTablero(datos) {
    var letra = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    var num = 9;
    var tablero = document.getElementById("tablero");
    var tabla = document.createElement("tabla");
    tabla.setAttribute("id", "tabla");
    tablero.appendChild(tabla);
    var tabla_body = document.createElement("tbody");
    tabla.appendChild(tabla_body);
    //asignar datos
    for (var i = 0; i < datos.length - 1; i++) {
      var tabla_row = document.createElement("tr");
      var fila = datos[i];
      var codePoint = fila.split("|");
      var tabla_dato = document.createElement("td");
      if (num != 9) tabla_dato.textContent = num;
      tabla_row.appendChild(tabla_dato);
      for (var j = 0; j < codePoint.length; j++) {
        var tabla_dato = document.createElement("td");
        if (codePoint[j] == '∅') {
          tabla_dato.textContent = "";
        } else
          tabla_dato.textContent = codePoint[j];

        if (i > 0) tabla_dato.setAttribute("id", letra[j] + num);
        tabla_row.appendChild(tabla_dato);

      }

      tabla_body.appendChild(tabla_row);
      num--;
    }
  }

  function mensajes(mensaje) {
    var div_mensaje = document.getElementById("mensaje");
    limparMensajes();
    var parrafo = document.createElement("p");
    parrafo.textContent = mensaje;
    div_mensaje.appendChild(parrafo);
  }
  return {
    "mostrarTablero": _mostrarTablero,
    "actualizarTablero": _actualizar_tablero,
    "moverPieza": _mover_pieza
  }

  function limparMensajes() {
    var div_mensaje = document.getElementById("mensaje");
    if (div_mensaje.childNodes[0] != null) {
      div_mensaje.removeChild(div_mensaje.childNodes[0]);
    }
  }

})();


//boton opcion
var opcion = document.getElementById("opcion");
var boton_option = document.createElement("button");
boton_option.textContent = "actualizar";
opcion.appendChild(boton_option);
boton_option.addEventListener('click', Ajedrez.actualizarTablero, true);
